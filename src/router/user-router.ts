import express from "express";
import * as homeController from "../controller/user";
import { Router } from "express";


const router = Router();
router.get('/', homeController.index);

// Export the router
export default router;