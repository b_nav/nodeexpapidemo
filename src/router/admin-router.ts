import * as adminController from "../controller/admin";
import { Router } from "express";
import { body } from "express-validator";
import * as Jwt from "../config/jwt";

const router = Router();

router.post('/login', [
    body('username').exists(),
    body('password').exists(),

], adminController.adminLogin);

router.post('/user', [Jwt.verifyToken, body('username').exists(),
body('ischeckedin').exists()], adminController.addUser)

router.get('/user', [Jwt.verifyToken], adminController.getUserList)
router.put('/user', [Jwt.verifyToken], adminController.updateUser)
// Export the router
export default router;