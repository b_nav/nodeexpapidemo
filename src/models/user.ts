import mongoose from "mongoose";
import bcrypt from 'bcrypt';
const userSchema = new mongoose.Schema(
    {
        firstname: String,
        lastname: String,
        checkin: Boolean,
        password: String
    },
    { timestamps: true }
);

export const passwordHash = function (candidatePassword: string) {
    return new Promise(function (resolve, reject) {
        bcrypt.genSalt(10, function (err, salt) {
            bcrypt.hash(candidatePassword, salt, function (err, hash) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(hash);
                }
            })
        })
    })
};

export const User = mongoose.model("User", userSchema);

