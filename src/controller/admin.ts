
import { Response, Request, NextFunction } from "express";
import { validationResult } from "express-validator";
import * as Jwt from "../config/jwt";
import { User, passwordHash } from '../models/user';
import { userData } from '../models/interfaces';
import mongoose from "mongoose";

export const adminLogin = (req: Request, res: Response) => {
    let errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    let { username, password } = req.body;
    let credentials = {
        username: 'test@admin.com',
        password: 'qwerty'
    }

    if (username === credentials.username && password === credentials.password) {
        let token = Jwt.generateJwt(username)
        return res.status(200).json({ msg: 'Admin Login', token, status: true });
    }
    else {
        return res.status(200).json({ msg: 'Invalid credentials', status: false });
    }
};

export const addUser = async (decoded: any, req: Request, res: Response, next: NextFunction) => {
    let errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    let { firstname, lastname, checkin, password } = req.body;
    password = await passwordHash(password);
    let user = new User({
        firstname,
        lastname,
        checkin,
        password
    });

    user.save(function (err: mongoose.Error) {
        if (err) {
            return res.status(200).json({ msg: 'error', status: false, err });
        }
        else {
            return res.status(200).json({ msg: 'data saved', status: true });
        }
    })
}


export const getUserList = (decoded: any, req: Request, res: Response, next: NextFunction) => {
    User.find({}, function (err, users) {
        if (err) {
            return res.send({ "status": false, "msg": "Error", err });
        }
        else {
            res.send({ "status": true, users });
        }
    });
}

export const updateUser = (decoded: any, req: Request, res: Response, next: NextFunction) => {
    let payload = req.body;
    let userid = mongoose.Types.ObjectId(payload._id);

    User.updateOne({ "_id": userid }, { $set: { "checkin": payload.checkin } }, function (err, result) {
        if (err) {
            return res.status(200).json({ "status": false, "msg": "Error in updating user", err });
        }
        else {
            return res.status(200).json({ "status": true });
        }
    });
}
