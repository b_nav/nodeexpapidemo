import express from "express";
import dotenv from "dotenv";
import mongoose from "mongoose";
import compression from "compression";
import bodyParser from "body-parser";

import { MONGODB_URI } from "./config/config";

import userRoutes from "./router/user-router";
import adminRoutes from "./router/admin-router";


const app = express();
dotenv.config({ path: ".env" });


mongoose.connect(MONGODB_URI, { useNewUrlParser: true }).then(
    () => { console.log("connected to mongo") },
).catch((err: any) => {
    console.log("MongoDB connection error. Please make sure MongoDB is running. " + err);
});


app.set("port", process.env.PORT || 3000);
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));



// routes
app.use('/api/v1', userRoutes);
app.use('/api/v1/admin', adminRoutes);

export default app;