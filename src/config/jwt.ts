import * as jwt from "jsonwebtoken";
import { Request, Response, NextFunction } from "express";
import { SESSION_SECRET } from "./config";
import { TokenData } from '../models/interfaces';

export const generateJwt = function (data: TokenData) {
    const expiresIn = Math.floor(Date.now() / 1000) + (60 * 60);
    return jwt.sign({
        data,
        exp: expiresIn,
    }, SESSION_SECRET);
}

export const verifyToken = function (req: Request, res: Response, next: NextFunction) {
    if (req.headers['authorization'] && req.headers['authorization'].split(' ')[0] === 'Bearer') {
        var token = req.headers['authorization']

        jwt.verify(token.split(' ')[1], SESSION_SECRET, function (err, decoded) {
            if (err) {
                return res.status(500).send({ auth: false, message: 'Failed to authenticate token.', err });
            }
            next(decoded);
        });
    }
    else {
        return res.status(400).send({ status: false, msg: 'No token provided' });
    }

}
