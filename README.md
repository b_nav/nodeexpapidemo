This app is for code demo.This app includes:

1. Admin login   
2. addUser api-  protected with jwt authentication, user password saved with hash
3. updateUser api protected with jwt authentication
4. getUserList api protected with jwt authentication
5. MongoDB as a database.

To run this app -->

 -->npm install -> node src/server.ts
 
 -->MongoDB at default port ---> 27017
 
Admin Credentials -->  user: test@admin.com , password: qwerty