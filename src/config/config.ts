import dotenv from "dotenv";

dotenv.config({ path: ".env" });

export const ENVIRONMENT = process.env.NODE_ENV;
const prod = ENVIRONMENT === "production"; // Anything else is treated as 'dev'


export const SESSION_SECRET = 'ash$5dfjhasdlkjfh78&alksdjhflak';
export const MONGODB_URI = prod ? 'mongodb://localhost:27017/demoapp' : 'mongodb://localhost:27017/demoapp';
